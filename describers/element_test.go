package describers

import (
	"bytes"
	"golang.org/x/net/html"
	"testing"
)

func TestElementDescriber(t *testing.T) {
	desc := ElementDescriber{
		Tag: `p`,
		Contents: []Describer{
			TextDescriber(`hello`),
		},
	}

	node := new(html.Node)

	if err := desc.Describe(node); err != nil {
		t.Fatal(err)
	}

	b := new(bytes.Buffer)
	html.Render(b, node)

	if b.String() != `<p>hello</p>` {
		t.Fatal(b.String())
	}
}
