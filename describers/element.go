package describers

import (
	"fmt"
	"golang.org/x/net/html"
	"golang.org/x/net/html/atom"
)

type ElementDescriber struct {
	Tag      string
	Contents []Describer
}

func (e ElementDescriber) Describe(target *html.Node) error {
	tag := atom.Lookup([]byte(e.Tag))

	if tag == 0 {
		return fmt.Errorf(`failed to find tag name: %s`, tag)
	}

	target.Type = html.ElementNode
	target.DataAtom = tag
	target.Data = tag.String()

	for _, content := range e.Contents {
		n := new(html.Node)
		content.Describe(n)

		if len(n.Attr) != 0 && n.Type == html.ErrorNode {
			content.Describe(target)
		} else {
			target.AppendChild(n)
		}
	}

	return nil
}
