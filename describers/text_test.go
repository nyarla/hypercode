package describers

import (
	"bytes"
	"golang.org/x/net/html"
	"testing"
)

func TestTextDescriber(t *testing.T) {
	desc := TextDescriber(`hello`)
	node := new(html.Node)

	if err := desc.Describe(node); err != nil {
		t.Fatal(`failed to describe html.Node tree by text node describer`)
	}

	b := new(bytes.Buffer)
	html.Render(b, node)

	if b.String() != `hello` {
		t.Fatal(b.String())
	}
}
