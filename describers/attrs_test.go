package describers

import (
	"bytes"
	"golang.org/x/net/html"
	"golang.org/x/net/html/atom"
	"testing"
)

func TestAttributesDescriber(t *testing.T) {
	desc := AttributesDescriber{`id`: `test`}
	node := &html.Node{
		Type:     html.ElementNode,
		DataAtom: atom.Div,
		Data:     `div`,
	}

	if err := desc.Describe(node); err != nil {
		t.Fatal(`failed to describe html.Node tree by attributes node describer`)
	}

	b := new(bytes.Buffer)
	html.Render(b, node)

	if b.String() != `<div id="test"></div>` {
		t.Fatal(b.String())
	}
}
