package describers

import (
	"golang.org/x/net/html"
)

type TextDescriber string

func (t TextDescriber) Describe(target *html.Node) error {
	target.Type = html.TextNode
	target.Data = string(t)

	return nil
}
