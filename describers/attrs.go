package describers

import (
	"golang.org/x/net/html"
)

type AttributesDescriber map[string]string

func (a AttributesDescriber) Describe(target *html.Node) error {
AttrKV:
	for k, v := range a {
		for idx, attr := range target.Attr {
			if attr.Key == k {
				target.Attr[idx].Val = v
				continue AttrKV
			}
		}

		target.Attr = append(target.Attr, html.Attribute{Key: k, Val: v})
	}

	return nil
}
