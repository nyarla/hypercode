package describers

import (
	"golang.org/x/net/html"
)

type Describer interface {
	Describe(target *html.Node) error
}

type DescriberFunc func(target *html.Node) error

func (d DescriberFunc) Describe(target *html.Node) error {
	return d(target)
}
