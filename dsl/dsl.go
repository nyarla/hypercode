package dsl

import (
	"github.com/nyarla/hypercode/describers"
	"golang.org/x/net/html"
)

type A map[string]string

func (a A) Describe(target *html.Node) error {
	return describers.AttributesDescriber(a).Describe(target)
}

type T string

func (t T) Describe(target *html.Node) error {
	return describers.TextDescriber(t).Describe(target)
}

func H(name string, contents ...describers.Describer) describers.Describer {
	tag, attr := ParseTagName(name)

	if attr != nil {
		contents = append(contents, attr)
	}
	return describers.ElementDescriber{Tag: tag, Contents: contents}
}
