package dsl

import (
	"bytes"
	"strings"

	"github.com/nyarla/hypercode/describers"
)

func ParseTagName(src string) (string, describers.Describer) {
	r := strings.NewReader(src)

	key := `tag`
	buf := new(bytes.Buffer)
	id := ``
	tag := `div`
	classes := make([]string, 0)

Read:
	for {
		ch, _, err := r.ReadRune()

		if ch == '#' || ch == '.' || err != nil {
			val := buf.String()

			if key == `id` {
				id = val
				goto Finalize
			}

			if key == `class` {
				for _, class := range classes {
					if class == val {
						goto Finalize
					}
				}

				classes = append(classes, val)
				goto Finalize
			}

			if key == `tag` {
				if val != `` {
					tag = val
				}
				goto Finalize
			}

		Finalize:
			buf = new(bytes.Buffer)

			if ch == '#' {
				key = `id`
				continue Read
			}

			if ch == '.' {
				key = `class`
				continue Read
			}

			if err != nil {
				break Read
			}
		}

		buf.WriteRune(ch)
	}

	attr := make(A)

	if id != `` {
		attr[`id`] = id
	}

	if len(classes) > 0 {
		attr[`class`] = strings.Join(classes, ` `)
	}

	if len(attr) == 0 {
		return tag, nil
	}

	return tag, attr
}
