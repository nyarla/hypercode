package dsl

import (
	"bytes"
	"golang.org/x/net/html"
	"testing"
)

func TestDSL(t *testing.T) {
	desc := H(`p#msg`, T(`hello, world!`))
	node := new(html.Node)

	if err := desc.Describe(node); err != nil {
		t.Fatal(err)
	}

	b := new(bytes.Buffer)
	html.Render(b, node)

	if b.String() != `<p id="msg">hello, world!</p>` {
		t.Fatal(b.String())
	}
}
