package dsl

import (
	"golang.org/x/net/html"
	"testing"
)

func TestParseTagName(t *testing.T) {
	if tag, _ := ParseTagName(`p`); tag != `p` {
		t.Fatal(`failed to parse tag name.`)
	}

	if tag, attr := ParseTagName(`p#id`); tag != `p` {
		t.Fatal(`failed to parse tag name with id`)
	} else {
		n := new(html.Node)
		attr.Describe(n)

		if len(n.Attr) != 1 || n.Attr[0].Key != `id` || n.Attr[0].Val != `id` {
			t.Fatalf(`failed to parse tag name with id: %+v`, n.Attr)
		}
	}

	if tag, attr := ParseTagName(`p.foo.bar`); tag != `p` {
		t.Fatal(`failed to parse tag name with classes`)
	} else {
		n := new(html.Node)
		attr.Describe(n)

		if len(n.Attr) != 1 || n.Attr[0].Key != `class` || n.Attr[0].Val != `foo bar` {
			t.Fatalf(`failed to parse tag name with classes: %+v`, n.Attr)
		}
	}

	if tag, attr := ParseTagName(`#id.foo.bar`); tag != `div` {
		t.Fatalf(`failed to parse id and classes without tag name: %+v`, tag)
	} else {
		n := new(html.Node)
		attr.Describe(n)

		if len(n.Attr) != 2 || n.Attr[0].Key != `id` || n.Attr[0].Val != `id` || n.Attr[1].Key != `class` || n.Attr[1].Val != `foo bar` {
			t.Fatalf(`failed to parse id and classes without tag name: %+v`, n.Attr)
		}
	}
}
