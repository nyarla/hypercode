package main

import (
	"log"
	"strings"

	"golang.org/x/net/html"

	. "github.com/nyarla/hypercode/dsl"
)

func main() {
	desc := H(`p#msg`, T(`hello, world!`))
	node := new(html.Node)

	if err := desc.Describe(node); err != nil {
		log.Fatal(err)
	}

	b := new(strings.Builder)
	html.Render(b, node)

	log.Println(b.String()) // => <p id="msg">hello, world!</p>
}
