hypercode
=========

  * A HTML as code library for golang is inspired from hyperscript.

CI Status
---------

  * GitLab CI: ![GitLab CI Status](https://gitlab.com/nyarla/hypercode/badges/master/build.svg)

SYNOPSIS
--------

```go
package main

import (
	"log"
	"strings"

	"golang.org/x/net/html"

	. "github.com/nyarla/hypercode/dsl"
)

func main() {
	desc := H(`p#msg`, T(`hello, world!`))
	node := new(html.Node)

	if err := desc.Describe(node); err != nil {
		log.Fatal(err)
	}

	b := new(strings.Builder)
	html.Render(b, node)

	log.Println(b.String()) // => <p id="msg">hello, world!</p>
}
```

LICENSE
-------

```
The MIT License (MIT)

Copyright © 2018 Naoki OKAMURA <nyarla@thotep.net>

Permission is hereby granted, free of charge, to any person obtaining a copy of this software
and associated documentation files (the “Software”), to deal in the Software without
restriction, including without limitation the rights to use, copy, modify, merge, publish,
distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or
substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

```
